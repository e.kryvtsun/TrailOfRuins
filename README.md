### **Trail Of Ruins** is a small non-commercial game prototype of 3D puzzle game for android mobile platform in fantasy genre.

> Development time: **30 hours**. 

> Tools used: **Blueprints / Mixamo / Epic Games Marketplace**

> At the following link you can watch a short gameplay video https://youtu.be/0lZZs_KORTI

At the moment there are only two levels in the game: the main menu and one game level.

![Trail of Ruins is a fantasy puzzle game](Screenshots/HighresScreenshot00000.png)

The player's task is to lead the protagonist from point A to point B, competently distributing secondary characters on pressure plates. The player can take control individually on every character.

![Game location](Screenshots/HighresScreenshot00002.png)

The main mechanics at the moment is the interaction of characters with pressure plates on level. In the future, there are plans to add new levels and mechanics, and develop the user interface (UI) component.

![On the level there are minor characters that are used to open doors](Screenshots/HighresScreenshot00003.png)

Working on the project I learned about several game optimization techniques, such as competent use of LOD groups, adjusting resolution and texture compression for game models depending on their importance, as well as baking lighting using Lightmass Importance Volume.

![Optimization for the android platform is an interesting and difficult task](Screenshots/HighresScreenshot00006.png)

An interesting challenge was to create a camera control system that could not only follow the movement of the active character through the level, but also move itself after him. To solve this problem I created my own CameraVolume class, in which I placed three elements - a camera, a trigger zone that checks if the active character has entered it and a spline component that is used to move the camera.

![The project was written using Blueprints](Screenshots/HighresScreenshot00009.png)


The project is still in the development stage and consumption of further "polishing". However, the knowledge and skills I have acquired while resolving emerging issues and conflicts, I am confident, will help me effectively perform work tasks.
